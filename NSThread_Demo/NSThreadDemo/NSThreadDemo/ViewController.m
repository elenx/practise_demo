//
//  ViewController.m
//  NSThreadDemo
//
//  Created by Darwin on 2017/1/21.
//  Copyright © 2017年 Darwin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, weak) UIImageView *imgV;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    UIImageView *imgV = [[UIImageView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:imgV];
    self.imgV = imgV;
    
    NSThread *thread = [[NSThread alloc] initWithTarget:self selector:@selector(download) object:nil];
    
    [thread start];
    
}

- (void)download {
    NSURL *url = [NSURL URLWithString:@"http://pic.nipic.com/2008-06-26/2008626164241438_2.jpg"];
    NSData *data = [NSData dataWithContentsOfURL:url];
    UIImage *img = [UIImage imageWithData:data];
    
    
    
    [self performSelectorOnMainThread:@selector(reloadImage:) withObject:img waitUntilDone:NO];
    
}

- (void)reloadImage:(UIImage *)img {
    self.imgV.image = img;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
